#include <avr/io.h>
#include <util/delay.h>
#define F_CPU 16000000UL
#define SEC 5

void _7seg_display (char*ones,char*tens,int DisplayTime);


void main()
{

 DDRC=0XFF;
 char ones[10]={0x04,0x14,0x24,0x34,0x44,0x4,0x64,0x4,0x84,0x94};
 char tens[10]={0x08,0x18,0x28,0x38,0x48,0x58,0x68,0x78,0x88,0x98};
 
 _7seg_display (ones,tens,1/*delay time*/);

}



void _7seg_display (char*ones,char*tens,int DisplayTime)
{
while (1)		// infinity loop
	{
	for(int j=0;j<10;++j)		// loop for the 2nd 7seg unit 
		{
		for(int i=0;i<10;++i)	// loop for the 1st 7seg unit 
			{	
			int k=0;			// initializing the counter k =0
			while (k<(500*DisplayTime /*sec*/))	//a single loop takes almost 2 ms 
				{	
					PORTC =ones[i];		//display the first 7seg unit 
					  _delay_ms(1);		// wait 1ms to allow the human eye to see 
					PORTC =tens[j];		//display the second 7seg unit 
					  _delay_ms(1);		// wait 1ms to allow the human eye to see 
					k++;			//counter increment 
				} 	
			}
		}
	}
}