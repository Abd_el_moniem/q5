/*
DIO_TYPES.h
Created on :30/9/2019
Author     :Monime
*/


#include "7SEG.h"

void _7seg_display (u16*ones,u16*tens,u16 DisplayTime){
 while (1)					// infinity loop
	{
	for(u16 j=0;j<10;++j)						// loop for the 2nd 7seg unit 
		{
		for(u16 i=0;i<10;++i)					// loop for the 1st 7seg unit 
			{	
			u16 k=0;							// initializing the counter k =0
			while (k<(500*DisplayTime /*sec*/))	// the single loop take 2 ms 
				{	
					_7seg_display_PORT =ones[i];	//display the first 7seg unit 
					  _delay_ms(1);					// wait 1ms to allow the human eye to see 
					_7seg_display_PORT =tens[j];	//display the second 7seg unit 
					  _delay_ms(1);					// wait 1ms to allow the human eye to see 
					k++;							//counter increment 
				} 	
			}
		}
	}
}



void initi_7seg_content(u16 *first_arr,u16 *seconed_arr){
 
 for (DIO_Channel_Type channel=Dio_Channel_C0 ; channel<Dio_Channel_D0; ++channel)
{								// loop on the DDRC channels
 DIO_Direction(channel,STD_LOW);// set DDR to low C is PORT 
}

 u16 comd=0;							// initializing variable to carry masked data from loop 

 for (u16 i =0 ; i<9; ++i){				//loop to store commend to de displayed at 7seg unites 
	   	comd =(i<<4);					// move the counter value to the second nipple
	   	SET_BIT(comd,Enable_7seg_1);	// enable the first 7seg unite
		first_arr[i]=comd 				//moving the commend to the array
	}
	comd=0;									// initializing variable to carry masked data from loop
 for (u16 i =0 ; i<9; ++i){	//loop to store commend to de displayed at 7seg unites 
	   	comd =(i<<4);						// move the counter value to the second nipple
	   	SET_BIT(comd,Enable_7seg_1);		// enable the second 7seg unite
		first_arr[i]=comd 					//moving the commend to the array
	}
}