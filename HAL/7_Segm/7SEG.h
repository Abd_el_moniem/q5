/*
DIO_TYPES.h
Created on :30/9/2019
Author     :Monime
*/

#ifndef 7SEG_H
#define 7SEG_H
#include <util/delay.h>
#define F_CPU 16000000UL

#include "../../LIB/BIT_MATH.h"
#include "../../LIB/STD_TYPES.h"

#include "../../MCAL/DIO/DIO.h"
#include "../../MCAL/DIO/DIO_HW.h"
#include "../../MCAL/DIO/DIO_TYPES.h"


#define _7seg_display_PORT PORTC_REG
#define Enable_7seg_1	3
#define Enable_7seg_2	4


void initi_7seg_content(u16 *first_arr,u16 *seconed_arr);
void _7seg_display (u16*ones,u16*tens,u16 DelayTime);


#endif