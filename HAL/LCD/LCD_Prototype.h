
/*
  LCD_Prototype.h
   Created: 30/9/2019
   Author: Moniem
 */ 



void LCD_Init (void) ;
void LCD_Command( unsigned char cmnd );
void LCD_Char( unsigned char data );
void LCD_String (char *str);
void LCD_String_xy (char row, char pos, char *str);
void LCD_Clear();
void LCD_fill_custom();
