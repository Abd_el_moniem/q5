



#define LCD_Dir				  DDRA			/* Define LCD data port direction */
#define LCD_Port			  PORTA			/* Define LCD data port */
#define LCD_Dir_Control		  DDRB			/* Define LCD Control port direction */
#define LCD_Port_Control	  PORTB			/* Define LCD Control port direction */

#define LCDClear() LCD_Command(0x01)


#define RS					  1				/* Define Register Select pin */
#define EN					  3
#define RW					  2				/* Define Register Select pin */
