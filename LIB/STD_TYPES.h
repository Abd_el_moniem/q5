/*
DIO.h
Created on 29/9/2019
Author Monime
*/

#ifndef _STD_Types_H
#define _STD_Types_H

#define ENABLE 					1
#define DISABLE					0

#define HIGH 					1
#define LOW						0

typedef unsigned		char	u8 ;
typedef signed			char	s8 ;
typedef unsigned short 	int		u16;
typedef signed	 short 	int		s16;
typedef unsigned long 	int		u32;
typedef signed   long 	int		s32;
typedef 				float	f32;
typedef 				double	f64;


typedef enum
{
	STD_LOW=0,
	STD_HIGH=!STD_LOW
}STD_levelType;



#endif 
