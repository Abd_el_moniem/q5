
/*
DIO.h
Created on 29/9/2019
Author Monime
*/

#ifndef _BIT_MATH
#define _BIT_MATH
	
#define SET_BIT(reg,bitno)			reg|=(1<<bitno)
#define CLR_BIT(reg,bitno)			reg &=~(1<<bitno)
#define GET_BIT(reg,bitno)			((reg>>bitno)&1)
#define TOGGLE_BIT(reg,bitno)		reg^=(1<<bitno)

#define MASK(BIT)      				   BIT&1

#endif