
#ifndef EXT_INTER_H
#define EXT_INTER_H

#include "../../LIB/BIT_MATH.h"
#include "../../LIB/STD_TYPES.h"

#define Enable_External_Interrupt 	1
#define Disable_External_Interrupt 	0

//General Interrupt Control Register (GICR) 
#define INT0 	6		
#define INT1	7		
#define INT2	5	


//MCU Control Register (MCUCR)		
#define ISC00	0		
#define ISC01	1	 	
#define ISC10	2	 	
#define ISC11	3	 	

//General Interrupt Flag Register (GIFR)
#define INTF0	Dio_Channel_D2		// PD2 
#define INTF1	Dio_Channel_D3		// PD3
#define INTF2	Dio_Channel_B2		// PB2

//Status Register  (SREG) 
#define Global_Interrupt_Enable		SET_BIT(SREG,7)
#define Global_Interrupt_Disable	CLR_BIT(SREG,7)


#endif


/*
Steps to configure the Interrupts:

1-  Set INTxand INTx bits in the General Interrupt Control Register (GICR)

2-  Configure MCU Control Register (MCUCR) to select interrupt type.

3-  Set Global Interrupt(I-bit) Enable bit in the AVR Status Register(SREG) 
	Handle the interrupt in the Interrupt Service Routine code.

*/