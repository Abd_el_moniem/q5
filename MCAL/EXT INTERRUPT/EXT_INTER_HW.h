/*
DIO_TYPES.h
Created on :30/9/2019
Author     :Monime

check ATmega32(L) Data sheet
	 “Register Summary” on page 327
*/

#ifndef EXT_INTER_HW
#define EXT_INTER_HW


#define SREG	*((volatile u8*)0x5F) //Status Register 
#define GICR	*((volatile u8*)0x5B) //General Interrupt Control Register 
#define GIFR	*((volatile u8*)0x5A) //General Interrupt Flag Register

#define MCUCR	*((volatile u8*)0x55) //General Interrupt Flag Register
#define MCUCSR	*((volatile u8*)0x54) //General Interrupt Flag Register

#endif