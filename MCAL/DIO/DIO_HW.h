/*
 DIO_HW.h
   Created: 29/9/2019
   Author: Monime

check ATmega32(L) Data sheet
	 “Register Summary” on page 327

 */

#include "../../LIB/STD_TYPES.h"

#ifndef DIO_HW_H
#define DIO_HW_H

//			A REG (PORT - PIN -	DDR)

#define PORTA_REG	*((volatile u8*)	0x3B )
#define DDRA_REG	*((volatile u8*)	0x3A )
#define PINA_REG	*((volatile u8*)	0x39 )
					//derf	//pointer //address
//			B REG (PORT - PIN -	DDR)


#define PORTB_REG	*((volatile u8*)	0x38 )
#define DDRB_REG	*((volatile u8*)	0x37 )
#define PINB_REG	*((volatile u8*)	0x36 )

//			C REG (PORT - PIN -	DDR)

#define PORTC_REG	*((volatile u8*)	0x35 )
#define DDRC_REG	*((volatile u8*)	0x34 )
#define PINC_REG	*((volatile u8*)	0x33 )

//			D REG (PORT - PIN -	DDR)

#define PORTD_REG	*((volatile u8*)	0x32 )
#define DDRD_REG	*((volatile u8*)	0x31 )
#define PIND_REG	*((volatile u8*)	0x30 )


#endif /* DIO_HW_H_ */
