/*
DIO_TYPES.h
Created on :29/9/2019
Author     :Monime
*/

#ifndef DIO_H
#define DIO_H
//function to write into the DDR Reg.
void DIO_Direction(DIO_Channel_Type channel,STD_levelType level);

//function to write into the PORTs Reg.
void DIO_WriteChannel(DIO_Channel_Type channel,STD_levelType level);

//function to read from the PIN Reg.
void DIO_ReadChannel(DIO_Channel_Type channel,u8 *date);

#endif /* DIO_H */
