/*
DIO_TYPES.h
Created on :29/9/2019
Author     :Monime
*/

#ifndef DIO_Types
#define DIO_Types

typedef enum
{
//REG_A
  Dio_Channel_A0,
  Dio_Channel_A1,
  Dio_Channel_A2,
  Dio_Channel_A3,
  Dio_Channel_A4,
  Dio_Channel_A5,
  Dio_Channel_A6,
  Dio_Channel_A7,
//REG_B
  Dio_Channel_B0,
  Dio_Channel_B1,
  Dio_Channel_B2,
  Dio_Channel_B3,
  Dio_Channel_B4,
  Dio_Channel_B5,
  Dio_Channel_B6,
  Dio_Channel_B7,
//REG_C
  Dio_Channel_C0,
  Dio_Channel_C1,
  Dio_Channel_C2,
  Dio_Channel_C3,
  Dio_Channel_C4,
  Dio_Channel_C5,
  Dio_Channel_C6,
  Dio_Channel_C7,
//REG_D
  Dio_Channel_D0,
  Dio_Channel_D1,
  Dio_Channel_D2,
  Dio_Channel_D3,
  Dio_Channel_D4,
  Dio_Channel_D5,
  Dio_Channel_D6,
  Dio_Channel_D7
}DIO_Channel_Type;

typedef enum
{
  DIO_PORT_A,
  DIO_PORT_B,
  DIO_PORT_C,
  DIO_PORT_D
}DIO_PORT_Type;

typedef enum
{
  DIO_PIN_A,
  DIO_PIN_B,
  DIO_PIN_C,
  DIO_PIN_D,
}DIO_PIN_Type;

typedef enum
{
  DIO_DDR_A,
  DIO_DDR_B,
  DIO_DDR_C,
  DIO_DDR_D,
}DIO_DDR_Type;


#endif
