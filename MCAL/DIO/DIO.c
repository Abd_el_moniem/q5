/*
DIO_TYPES.h
Created on :29/9/2019
Author     :Monime
*/

#include "../../LIB/STD_TYPES.h"
#include "../../LIB/BIT_MATH.h"
#include "DIO_TYPES.h"
#include "DIO_HW.h"

//function to set the DDR 
void DIO_Direction(DIO_Channel_Type channel,STD_levelType level){
	
	DIO_DDR_Type 	 DDR_select = channel/8;// setting the DDR Reg number 
	DIO_Channel_Type DDR_Bit 	= channel%8;// setting the Bit inside the Reg.
 switch (DDR_select){	

	 case DIO_DDR_A:
		if (level==LOW)					//if the input =low(0)
			CLR_BIT(DDRA_REG,DDR_Bit);	// make  DDR 0 (PIN)
		else if(level==HIGH)			//if input = HIGH (1)
			SET_BIT(DDRA_REG,DDR_Bit);	// make  DDR 1 (PORT)
	 break;

	 case DIO_DDR_B:
		if (level==LOW)					//if the input =low(0)
			CLR_BIT(DDRB_REG,DDR_Bit);	// make  DDR 0 (PIN)
		else if(level==HIGH)			//if input = HIGH (1)
	 		SET_BIT(DDRB_REG,DDR_Bit);	// make  DDR 1 (PORT)
	 break;

	 case DIO_DDR_C:
		if (level==LOW)					//if the input =low(0)
			CLR_BIT(DDRC_REG,DDR_Bit);	// make  DDR 0 (PIN)
		else if(level==HIGH)			//if input = HIGH (1)
			SET_BIT(DDRC_REG,DDR_Bit);	// make  DDR 1 (PORT)
	 break;

	 case DIO_DDR_D:
		if (level==LOW)					//if the input =low(0)
			CLR_BIT(DDRD_REG,DDR_Bit);	// make  DDR 0 (PIN)
		else if(level==HIGH)			//if input = HIGH (1)
			SET_BIT(DDRD_REG,DDR_Bit);	// make  DDR 1 (PORT)
	 break;
	}
}


void DIO_WriteChannel(DIO_Channel_Type channel,STD_levelType level){
	
	DIO_PIN_Type 	 PIN_select = channel/8;// setting the PIN Reg number 
	DIO_Channel_Type PIN_Bit 	= channel%8;// setting the Bit inside the Reg.
 switch (PIN_select){
 
	 case DIO_PIN_A:
		if (level==LOW)
			CLR_BIT(DIO_PIN_A,PIN_Bit);
		else if(level==HIGH)
			SET_BIT(DIO_PIN_A,PIN_Bit);
	 break;

	 case DIO_PIN_B:
	 	if (level==LOW)
			CLR_BIT(DIO_PIN_B,PIN_Bit);
		else if(level==HIGH)
			SET_BIT(DIO_PIN_B,PIN_Bit);
	 break;

	 case DIO_PIN_C:
	 	if (level==LOW)
			CLR_BIT(DIO_PIN_C,PIN_Bit);
		else if(level==HIGH)
			SET_BIT(DIO_PIN_C,PIN_Bit);
	 break;

	 case DIO_PIN_D:
		if (level==LOW)
			CLR_BIT(DIO_PIN_D,PIN_Bit);
		else if(level==HIGH)
			SET_BIT(DIO_PIN_D,PIN_Bit);
	 break;
	}
}



void DIO_ReadChannel(DIO_Channel_Type channel,u8 *data){
	
	DIO_PORT_Type 	 PORT_select = channel/8;// setting the PORT Reg number 
	DIO_Channel_Type PORT_Bit 	 = channel%8;// setting the Bit inside the Reg.

 switch (PORT_select){
 
	 case DIO_PORT_A:
		*data=GET_BIT(PORTA_REG,PORT_Bit);//write pass the data from the bit of the PORT to the address of the parameter data  
	 break;

	 case DIO_PORT_B:
	 	*data=GET_BIT(PORTB_REG,PORT_Bit);//write pass the data from the bit of the PORT to the address of the parameter data  
	 break;

	 case DIO_PORT_C:
	 	*data=GET_BIT(PORTC_REG,PORT_Bit);//write pass the data from the bit of the PORT to the address of the parameter data  
	 break;

	 case DIO_PORT_D:
		*data=GET_BIT(PORTD_REG,PORT_Bit);//write pass the data from the bit of the PORT to the address of the parameter data  
	 break;
	}
}
